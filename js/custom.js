$(document).ready(function(){

   
   $(function(){
     activate('img[src*=".svg"]');
 
     function activate(string){
         jQuery(string).each(function(){
             var $img = jQuery(this);
             var imgID = $img.attr('id');
             var imgClass = $img.attr('class');
             var imgURL = $img.attr('src');
 
             jQuery.get(imgURL, function(data) {
                 // Get the SVG tag, ignore the rest
                 var $svg = jQuery(data).find('svg');
 
                 // Add replaced image's ID to the new SVG
                 if(typeof imgID !== 'undefined') {
                     $svg = $svg.attr('id', imgID);
                 }
                 // Add replaced image's classes to the new SVG
                 if(typeof imgClass !== 'undefined') {
                     $svg = $svg.attr('class', imgClass+' replaced-svg');
                 }
 
                 // Remove any invalid XML tags as per http://validator.w3.org
                 $svg = $svg.removeAttr('xmlns:a');
 
                 // Replace image with new SVG
                 $img.replaceWith($svg);
 
             }, 'xml');
         });
     }
 
 
 });


 $('.language span').click(function () {
    $('.language span').removeClass('active');
    $(this).addClass('active');
  });

 $('.open-request').click(function () {
    $('#modal-request').addClass('active');
  });

 $('.modal .overlay').click(function () {
    $('.modal').removeClass('active');
  });
 $('.modal .close').click(function () {
    $('.modal').removeClass('active');
  });
 $('.modal .okay').click(function () {
    $('.modal').removeClass('active');
  });




  var hamburger = $('.hamburger');
  var mobnav = $('#mob-nav');

  hamburger.on('click', function() {
    $(this).toggleClass('active');
    $(this).toggleClass('not-active');
    $(mobnav).toggleClass('active');
  });


  $('#mob-nav a').click(function () {
    $(mobnav).removeClass('active');
    $(hamburger).removeClass('active');
    $(hamburger).addClass('not-active');
  });


 }); 
 
 
 
 var $screens = $('.gallery-images');
 $screens.slick({
     arrows: true,
     dots: true,
     infinite: true,
     draggable: true,
     rows: 2,
     slidesPerRow: 3,
     prevArrow: $('.gallery-nav .prev'),
     nextArrow: $('.gallery-nav .next'),
     appendDots: $('.gallery-nav .dots'),
     centerPadding: '40px',
     responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            dots: true,
            
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesPerRow: 1,
            centerMode: false,
            variableWidth: true
          }
        }
      ]
     });




      $('#modal-submit').click(function(){
      var validateForm = $(document).find('#mainform');
      validateForm.validate({
          rules: {
              name: {
                  required: true,
                  minlength: 2
              },
              phone: {
                  required: true,
                  number: true,
                  minlength: 10
              }
          },
          messages: {
              name: {
                  required: "Введите ваше имя",
                  minlength: "Имя должно быть более 2-х символов"
              },
              phone: {
                  required: "Введите номер телефона",
                  minlength: "Поле должно быть более 10-ти символов",
                  number: "Неправильный номер"
              }
          },
          submitHandler: function (form) {
              var formData = $(form).serialize();
              $.ajax({
                  url: "sendform.php",
                  type: "post",
                  data: formData,
                  beforeSend: function () {
                  },
                  success: function (data) {
                    $('.modal-content.main').removeClass('active');
                    $('.modal-content.submitted').addClass('active');
                  }
              });
              
          },
          errorPlacement: function(error, element) {                 
              var item = element.parents('.item');
              item.append(error);
          }
  
      });
   
      validateForm.submit();
  }); 